# Python ROS version of the Livox Python driver

Forked from https://github.com/ryan-brazeal-ufl/OpenPyLivox

Added ROS simple example

This package requires PYTHON3, which ROS Melodic does not support.

1. Create virtual environment. This example uses conda 
```
conda env create -f environment.yml
```

2. Install catkin_pkg, rospkg and ros_numpy, in that order:
```
cd catkin_pkg && python setup.py install && cd ..
cd rospkg && python setup.py install && cd ..
cd ros_numpy && python setup.py install && cd ..
python setup.py install
```

## How to run

```
usage: livox_controller_demo.py [-h] -H HOST_IP -s SENSOR_IP -d DATA_PORT -c
                                CMD_PORT -i IMU_PORT -t TOPIC_NAME -f FRAME_ID
                                -n NODE_NAME

Livox test

optional arguments:
  -h, --help            show this help message and exit
  -H HOST_IP, --host_ip HOST_IP
                        Host PC IP Address where to listen
  -s SENSOR_IP, --sensor_ip SENSOR_IP
                        Lidar Sensor IP
  -d DATA_PORT, --data_port DATA_PORT
                        UDP port used to receive lidar data
  -c CMD_PORT, --cmd_port CMD_PORT
                        UDP port used to send commands
  -i IMU_PORT, --imu_port IMU_PORT
                        UDP port used to receive imu data
  -t TOPIC_NAME, --topic_name TOPIC_NAME
                        Topic where to publish the cloud
  -f FRAME_ID, --frame_id FRAME_ID
                        Coordinate frame id to include in the header
  -n NODE_NAME, --node_name NODE_NAME
                        ROS Node name

```
### Example

```
python livox_controller_demp.py -H 192.168.1.100 \
        -s 192.168.1.168 \ 
        -d 23000  \
        -c 24000  \
        -i 25000 \
        -t livox1 \
        -f livox \
        -n livox1
```
---

# Original README

<img width="275px" src="./images/OPL_logo2_sm.png">

<hr>

# OpenPyLivox (OPL)
The ***unofficial*** Python3 driver for Livox lidar sensors ;)

The OpenPyLivox (OPL) library is a near complete, fully pythonic, implementation of the Livox SDK. This means that almost all the functionality available within official Livox software (e.g., Livox-Viewer) and their C++ based API, has been included here within the OpenPyLivox library. *Ok, ok ... maybe not quite as cool or as functional as the Livox-Viewer, ... yet!*

This library and associated documentation, is designed for **ANYONE and EVERYONE** to be able to use it. From students and teachers interested in using lidar/laser scanning in STEM curriculum or projects, to researchers investigating lidar for autonomous vehicle/robot navigation!

***See the [Wiki](../../wiki) Pages for ~~complete~~ documentation!***
 - Documentation is currently being updated for the new features of v1.1.0, view the demo file below for how to access the new features.

Check out the [livox_controller_demo.py](./livox_controller_demo.py) file for a very detailed example on how to use the OpenPyLivox library to control and capture point cloud data from a single Livox sensor, as well as from multiple Livox sensors simultaneously!

*NOTES:* 
- OPL v1.1.0 has **NOW** been tested using Mid-40, Mid-100, and Horizon sensors (the Tele-15 should also work, but the developer does not own one)
- OPL automatically connects to all 3 individual Mid-40 sensors within a Mid-100 device, however, it was decided NOT to merge the point cloud datasets (i.e., there are 3 seperate files for a single Mid-100 device)
- Simultaneous operation of multiple Livox sensors has been tested, but not using a Livox Hub
- The library has been tested on Mac OS X, Linux (GalliumOS on HP Chromebook), and Windows 7 and 10
- The library has been tested to work with Livox firmwares:
  *MID-40/MID-100*
  - 03.03.0001 to 03.03.0007, all [special Livox firmwares](https://github.com/Livox-SDK/Special-Firmwares-for-Livox-LiDARs), including multiple returns ;)
  - 03.05.0000 to 03.08.0000, standard versions
  *HORIZON
  - 06.08.0000 to 06.10.0000, standard version
- OPL stored binary point data can be converted to CSV and/or LAS files
- OPL stored binary IMU data can be converted to CSV files
- The CSV and LAS point cloud data to be easily opened in the <b>amazing</b> open source software, CloudCompare (download at https://cloudcompare.org)

**Quirky Fact:** Intensity (a.k.a., Reflectivity in the Livox documentation) has a hyphen in the middle of it for OPL CSV point clouds in order to 'trick' CloudCompare into assigning the field as a scalar type by default. This enables displaying the point cloud in CloudCompare using a more visually appealing colour spectrum (e.g., left image below). It also provides a way to (possibly) help filter out some unwanted noisy data. Of course, the colour scheme can be changed to many other options, after importing the point cloud in CloudCompare (e.g., greyscale in right image below)

<table style="border:0px;">
  <tr style="border:0px;">
    <td style="border:0px;"><img src="./images/image1_rs.png"></td>
    <td style="border:0px;"><img src="./images/image2_rs.png"></td>
  </tr>
</table>
<table style="border:0px;">
  <tr style="border:0px;">
   <td style="border:0px;"><img width="700px" src="./images/Horizon_FOV.jpg"></td>
   <td style="border:0px;"><img width="185px" src="./images/IMG_2072.jpeg"></td>
  </tr>
</table>

## Change Log:
- v1.0 released, **FRIDAY** Sept. **the 13th** 2019
- Wiki for v1.0 completed, Monday Sept. 23rd 2019
- Wiki v1.0 minor updates, Saturday Nov. 30th 2019
- v1.0.1 released, Wednesday May 27th 2020
- v1.0.2 and v1.0.3 released, Friday May 29th 2020
- v1.1.0 released, Friday Sept. 11th 2020 (NEVER FORGET!)

