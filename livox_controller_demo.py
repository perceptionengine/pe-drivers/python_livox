#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import time
import argparse
import openpylivox as opl
import rospy
from sensor_msgs.msg import PointCloud2


def main(host_ip, sensor_ip, data_port, cmd_port, imu_port, topic_name, frame_id, node_name):
    rospy.init_node(node_name)
    publisher=rospy.Publisher(topic_name, PointCloud2, queue_size=1)
    sensor = opl.openpylivox(True, topic_name, frame_id, publisher)

    sensor.showMessages(True)
    sensor.resetShowMessages()

    connected = sensor.connect(host_ip, sensor_ip,  data_port,  cmd_port,  imu_port)

    # make sure a sensor was connected
    if connected:
        connParams = sensor.connectionParameters()
        firmware = sensor.firmware()
        serial = sensor.serialNumber()

        sensor.showMessages(True)
        sensor.resetShowMessages()
        sensor.lidarSpinUp()

        sensor.dataStart_RT_B()
        sensor.setIMUdataPush(True)

        sensor.saveDataToFile("ROS", 0.1, 20)
        while True:
            #time.sleep(10)

            if sensor.doneCapturing():
                break

        # stop data stream
        sensor.dataStop()

        # properly disconnect from the sensor
        sensor.disconnect()

    else:
        print("\n***** Could not connect to a Livox sensor *****\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Livox test')
    parser.add_argument('-H', '--host_ip', type=str, help='Host PC IP Address where to listen', required=True)
    parser.add_argument('-s', '--sensor_ip', type=str, help='Lidar Sensor IP', required=True)
    parser.add_argument('-d', '--data_port', type=int, help='UDP port used to receive lidar data', required=True)
    parser.add_argument('-c', '--cmd_port', type=int, help='UDP port used to send commands', required=True)
    parser.add_argument('-i', '--imu_port', type=int, help='UDP port used to receive imu data', required=True)
    parser.add_argument('-t', '--topic_name', type=str, help='Topic where to publish the cloud', required=True)
    parser.add_argument('-f', '--frame_id', type=str, help='Coordinate frame id to include in the header', required=True)
    parser.add_argument('-n', '--node_name', type=str, help='ROS Node name', required=True)

    args = parser.parse_args()
    main(args.host_ip, args.sensor_ip, args.data_port, args.cmd_port, args.imu_port, args.topic_name, args.frame_id, args.node_name)
